var tab = [  //creation du tableau 
    [0, 0, 0, 0, 0, 0], // Colonne 1
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
]
// com
function j1() { // fonction qui permet de recupere le nom du joueur 1
    var joueur1=prompt('Nom du joueur 1'); //la variable joueur1 prend le nom du joueur1
    alert("Bienvenue "+ joueur1 +"!"); //affiche le nom du joueur1
}

function j2() { // fait la meme chose que la foncltion j1
     var joueur2=prompt('Nom du joueur 2');
     alert("Bienvenue "+ joueur2 +"!");
}

var c = 0;
function comptor() { //permet de compter le nombre de fois qu'on clique dans une colonne
    c++;
  }
  function Pousser(numColumn){ //permet de faire siwtcher les jetons quand on clique sur la meme colonne
    var boxes = document.getElementsByClassName("col-" + numColumn);
    var col = tab[numColumn]; //créer une variable qui prend le numero de la colonne
    var position = col.indexOf(0);
    col[position] = 1 + (c % 2); // vaut 1 ou 2
    if (c % 2 == 0) {
       boxes[5 - position].insertAdjacentHTML('afterbegin', '<div class="token1"></div>');  // 5- position permet d'afficher les jetons par le bas
    }
    else {
        boxes[5 - position].insertAdjacentHTML('afterbegin', '<div class="token2"></div>');
    }
    console.log(tab);

    var winner = (checkWinnerColumn() || checkWinnerRow());  //creer une variable qui prend la fonction checkWinnerColumn ou la fonction checkWinnerRow
    if (winner) alert(winner) // si les conditions d'une des 2 fonctions sont vérifiés, on apelle la fonction
}

// Check alignement colonnes

function checkWinnerColumn() { //fonction qui permet de check si 4 jetons sont alignés en colonne
    // initialisation
    var token1Win = false;
    var token2Win = false;
    var numColumns = tab.length;
    var numRows = tab[0].length;
    // dans chaque colonne
    for (var i = 0; i <= 5; i++) {
        // dans chaque rangée
        for (var j = 0; j <= 5; j++) {
            // nos 4 jetons
            var tokenA = tab[i][j]; // jeton1
            var tokenB = tab[i][j + 1]; // jeton2
            var tokenC = tab[i][j + 2]; // jeton3
            var tokenD = tab[i][j + 3]; // jeton4
 
           // Est-ce que licorne gagne ?
            var token1Win = (
                tokenA === 1
                && tokenB === 1
                && tokenC === 1
                && tokenD === 1
            );
            // Est-ce que chrome gagne ?
            var token2Win = (
                tokenA === 2
                && tokenB === 2
                && tokenC === 2
                && tokenD === 2
            );

            // Alert
            if (token1Win) { return "Joueur 1 a gagné!"; }
            if (token2Win) { return "Joueur 2 a gagné!"; }
        }
    }
}

function checkWinnerRow() {  //fonction qui permet de check si 4 jetons sont alignés en ligne
    var token1Win = false;
    var token2Win = false;
    var numColumns = tab[0].length;
    var numRows = tab.length;

    // dans chaque colonne
    for (var i = 0; i <= 5; i++) {
        // dans chaque rangée
        for (var j = 0; j <= 5; j++) {


            // créer 4 variables pour 4 jetons
            var tokenA = tab[i][j]; // jeton1
            var tokenB = tab[i + 1][j]; // jeton2
            var tokenC = tab[i + 2][j]; // jeton3
            var tokenD = tab[i + 3][j]; // jeton4

            // check si les 4 variables sont de la valeur 1
            var token1Win = (
                tokenA === 1
                && tokenB === 1
                && tokenC === 1
                && tokenD === 1
            )

            // check si les 4 variables sont de la valeur 2
            var token2Win = (
                tokenA === 2
                && tokenB === 2
                && tokenC === 2
                && tokenD === 2
            )
            // Alert : si 4 variables sont de la meme valeur, on renvoit token1 ou 2
            if (token1Win) { return "token1" }
            if (token2Win) { return "token2" }
        }
    }
}
